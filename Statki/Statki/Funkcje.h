#pragma once

// ---------- MENU ----------
void menu();
void pokaz_autorow();
void instrukcja();
void powitanie();
// --------------------------

// ---------- GRA ----------
void GRA();
void legenda_pol();
void informacje_o_polach(int l1, int l2, int t);
void wygrana(int t);
void przegrana(int t);
// -------------------------

// ---------- Funkcje pomocnicze ----------
void zmiana_rozmiaru_okna(short szerokosc, short wysokosc);
void tekst_powrot_do_menu();
void kolor_tekstu(int c);
void wyczysc_ekran();
void stop();
// ----------------------------------------